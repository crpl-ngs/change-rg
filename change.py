import sys

the_line = "@RG\\tID:bench\\tLB:bench\\tSM:bench\\tPU:bench\\tPL:ILLUMINA"
the_file="output"
if len(sys.argv) > 1:
    the_file=sys.argv[1]
if len(sys.argv) > 2:
    the_line = sys.argv[2]
the_line = bytes(the_line, "utf-8").decode("unicode_escape")
if not the_line.endswith('\n'):
    the_line += '\n'

with open(the_file, "r") as f:
    for line in f:
        if line.startswith('@RG'):
            line = the_line
        print(line, end='')
